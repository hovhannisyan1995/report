<?php

namespace App\Http\Controllers;

use App\Report;
use Response;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();

        return response()->json([
            'reports' => $reports,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        return Report::where('id',$report)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }

    /**
     * @param Request $request
     * @param Report $report
     * @return \Illuminate\Http\JsonResponse
     */
    public function getReportApi(Request $request, Report $report)
    {
        $client = new Client();
        $token = 'AgAAAAA1xxRoAAbSN8EWYckpp0FkmXxwPeaI6lU';
        $headers = [
            'Authorization' => 'OAuth ' . $token,
            'Accept'        => 'application/json',
        ];
        $limit = 50;
        $offset = 0;
        $release_count = 1;

        do {
            //if ($offset != 0) dispatch;
            try {
                $data = array(
                    'lang' => 'ru',
                    'pretty' => '1',
                    'field' => array('shows', 'fillrate', 'hits'),
                    'dimension_field' => 'date|day',
                    'entity_field' => array('page_caption', 'complex_block_id', 'page_id', 'tag_id'),
                    'period' => '30days',
                    'limits' => '{"limit":'.$limit.',"offset":'.$offset.'}'

                );
                $query_string = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=',http_build_query($data,null,'&'));
                $response = $client->request('POST','https://partner2.yandex.ru/api/statistics2/get.json?'. $query_string,[
                                                'headers' => $headers
                                            ])->getBody()->getContents();
                $response = json_decode($response,true);
                $release_count += count($response['data']['points']);
                foreach ($response['data']['points'] as $report) {
                    $website  = $report['dimensions']['page_caption'];
                    $date  = $report['dimensions']['date'][0];
                    $impression  = $report['measures'][0]['shows'];
                    $revenue  = $report['measures'][0]['fillrate'];
                    $report = new Report;
                    $report->website = $website;
                    $report->currency = '$';
                    $report->impression = $impression;
                    $report->date = $date;
                    $report->revenue = $revenue;
                    $report->save();
                }
            } catch (\Exception $e) {
                print $e->getMessage();
            }
            $offset += $limit;
        } while ($offset < $release_count);



        return response()->json([
            "message" => "Report created successfully"
        ], 201);
    }
}
