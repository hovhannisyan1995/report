<?php
use Faker\Generator as Faker;

$factory->define(App\Report::class, function (Faker $faker) {
    return [
        'website' => $faker->word().'.'.$faker->randomElement(['ru','com']),
        'impression' => $faker->sentence(2),
        'revenue' => $faker->numberBetween(10,100),
        'date' => $faker->dateTimeBetween('now', '+1 years'), // secret
        'currency' => $faker->randomElement(['$','€','֏']),
    ];
});
