<?php

use Illuminate\Database\Seeder;

class ReportTableSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reports = factory(App\Report::class, 20)->create();
    }
}
