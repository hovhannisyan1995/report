
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('angular');
import $ from 'jquery';
import 'datatables.net-dt';
var app = angular.module('LaravelCRUD', []
    , ['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post['X-CSRF-TOKEN'] = $('meta[name=csrf-token]').attr('content');
    }]);

app.controller('ReportsController', ['$scope', '$http', function ($scope, $http) {
	$scope.reports = [];
    // List reports
    $scope.loadReports = function () {
        $http.get('/report')
            .then(function success(e) {
                $scope.reports = e.data.reports;
            });
    };
    $scope.loadReports();
}]);


