@extends('layouts.app')

@section('content')
<div class="container-fluid" ng-controller="ReportsController">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <table id="reportTable" class="display" style="width:100%" ng-if="reports.length > 0">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>ID</th>
                                    <th>Website</th>
                                    <th>Impression</th>
                                    <th>Revenue</th>
                                    <th>Date</th>
                                    <th>Currency</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="(index, report) in reports">
                                    <td>
                                        @{{ index + 1 }}
                                    </td>
                                    <td>@{{ report.id }}</td>
                                    <td>@{{ report.website }}</td>
                                    <td>@{{ report.impression }}</td>
                                    <td>@{{ report.revenue }}</td>
                                    <td>@{{ report.date }}</td>
                                    <td>@{{ report.currency }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>NO</th>
                                <th>ID</th>
                                <th>Website</th>
                                <th>Impression</th>
                                <th>Revenue</th>
                                <th>Date</th>
                                <th>Currency</th>
                            </tr>
                            </tfoot>
                            <script type="text/javascript">
                                $(document).ready( function () {
                                    $('#reportTable').DataTable();
                                } );
                            </script>
                        </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

